package com.kanin.robot;

public class Robot {
    public Robot(Map map,char symbol,int x,int y){
        super(map,symbol,x,y,false);

    }
    public void up(){
        int y = this.getY();
        y--;
        this.setY(y);
    }
    private void setY(int y) {
    }
    private int getY() {
        return 0;
    }
    public void down(){
        int y = this.getY();
        y++;
        this.setY(y);
    }
    public void left(){
        int x = this.getX();
        x--;
        this.setX(x);
    }
    private void setX(int x) {
    }
    private int getX() {
        return 0;
    }
    public void right(){
        int x = this.getX();
        x++;
        this.setX(x);
    }
}
